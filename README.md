# soutenance de stage Hadrien 2020

_premier brouillon: 30/07/2020_

## _commentaires_

objectif en temps: **30min** + 15min de questions
   --> 15/20min de contexte + 10/15min de contribs

_Jury --> pas trop mettre de code_

_stage entre théorie --> pratique_

## Plan

1. Introduction
    _très courte_
    - contexte (présentation du stage)
    - plan

2. Présentation du sujet

    - futurs
        - dessins
    - def
        - dessins aussi
    - Encore dans les grandes lignes et les principes

    - Motivation et plan
        - objectifs
        - contributions

3. Fut on Flow
    - Objectifs
    - Principe théorique
        - règle de flattening
        - typer le dessin
    - > On l'a fait
    - Conclusion

4. Benchmarks
    - Objectifs
    - Analyse de résultats
        - wordcounter (histoire avec les 2 tableaux)
        - linkedlist
    - Conclusions

5. Fut on flow 
    _(1 slide, rapide)_

5. Conclusion


### Commentaires sur le plan

#### Faut-il parler de forward ?

Dans mon rapport j'ai l'angle d'attaque de typer forward, mais je suis pas sûr de vouloir présenter ça

J'aimerais peut-être plus l'attaquer à partir du travail de Nicolas (ie return = forward*), pour pouvoir balancer dans la conclusion des benchmarks / la conclusion tout court que le mieux est de faire implicitement forward* à la place de return.

_Au moins le mentionner dans la conclusion. Préparer un slide indépendant dessus._

---> Conclusion: soit on le met dans le début de la conclusion, soit dans la fin du 3


