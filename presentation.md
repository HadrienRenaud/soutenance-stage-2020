# Soutenance de stage

Hadrien Renaud, 3 septembre 2020

Notes:
 - Lieu & dates du stage
 - Équipe + tuteur
 - Sur quoi ? Futurs
    - stage entre théorie et pratique

---

## Plan

1. Contexte
    1. Dataflow explicit Futures
    2. Délégation avec `forward`
2. Contributions
    1. Implémentation des control flow explicit futures 
    2. Benchmarks

Notes:
Ne pas dire les sous-parties

---

## Actor model

![Actor model](./schemas/actors-without-futures.svg)

Notes:
- acteur = entité
- entités communiquent avec des messages
- ici 3 acteurs, main est appelé au lancement du programme
- main appelle la méthode `foo` de `a`, et foo appelle la methode `bar` de `b`
- un appel
    - crée un futur, qui est donné à l'appelant
    - la méthode appelée est planifiée dans le thread de l'object appelé
    - quand elle retournera une valeur, celle-ci sera mise dans le futur

---

## Futurs

Futur non rempli

![Unfulfilled future](./schemas/unfulfilled-future.svg)

Futur rempli

![Fulfilled future](./schemas/fulfilled-future.svg)

Notes:
- Un futur est un proxy pour un résultat qui sera obtenu plus tard. Il est lié à une méthode, et une fois qu'il est rempli, il est en lecture seule.
- La structure d'un futur est refletée par sa structure une fois remplie, donc sur les schemas je ne mettrai que les structures remplies

---

## Acteurs & futurs

![Actor schema](./schemas/actors.svg)

Notes:
TODO: avoir une définition claire pour les futures

---

### Opérations sur les futurs

_(Control flow explicit futures)_

- `get`: synchronisation
- `a!m()`: appel asynchrone

```ruby
val f: Fut[int] = a!foo()
val x: int = get(f)
```

Notes:
D'autres opérations sont possibles en Encore, mais elles sont moins importantes

---

### Encore

- Compilateur (en Haskell) vers C
- Utilise le runtime de Pony

- déclaration d'une classe d'acteurs : `active class A`
- annotations de type: `a: A`
- un futur: `val f: Fut[int] = a!foo()`

Notes: un très bref mot sur Encore

---

### Problème 1: prolifération des futurs

```ruby
active class Broker
    def compute(arg: Arg): Fut[Result]
        get_worker()!compute(arg)
    end
end

def main(): unit
    val broker = new Broker
    val f: Fut[Fut[Result]] = broker!compute(new Arg)
    val result = get(get(f))
end
```

![Future chains](./schemas/future-proliferation.svg) <!-- .element: class="fragment" -->

Notes:
- On va créer des types Fut[Fut[T]] avec 2 niveaux, 3, ou même un nombre non borné de futurs
- Pas possible de typer une fonction asynchrone récursive

---

### Problème 2: prolifération des types

```ruby
active class CachedBroker
    def compute(arg: Arg): Result | Fut[Result]
        if cache.has(arg) then
            cache.get(arg)
        else
            get_worker()!compute(arg)
        end
    end
end
```

Notes:
- Non typable pour l'instant
- C'est problématique

---

## Solutions

1. Delegation with `forward`
1. Dataflow explicit futures

---

### Délégation des futurs

![return schema](./schemas/return.svg)

---

### Délégation des futurs

![forward schema](./schemas/forward.svg)

---

### Délégation des futurs --- définition

keyword : `forward`

```ruby
def broker(arg: Arg): Result
    forward(get_worker()!compute(arg))
end
```
<!-- .element class="fragment"-->


Notes:
 - syntaxe similaire à return

 - On va prendre un exemple pour comparer return et forward
 - C'est le même schema que l'exemple sur les acteurs:
   - 3 classes: A, B et main

---

### Délégation des futurs --- exemple

```ruby [|2,6,13,14]
active class CachedBroker
    def compute(arg: Arg): Result
        if cache.has(arg) then
            cache.get(arg)
        else
            forward(get_worker()!!compute(arg))
        end
    end
end

def main(): unit
    val broker = new Broker
    val f: Fut[Result]] = broker!compute(new Arg)
    val result = get(f)
end
```

Notes:
 maintenant constant en espace ! 

---

### Dataflow explicit futures --- définition

![getstar](./schemas/getstar.svg)

"`Flow[T] := Fut$^\star$[T]`"
<!-- .element: class="fragment" -->

`Flow[Flow[T]]` `$\rightarrow$` `Flow[T]`
<!-- .element: class="fragment" -->

Notes: 
Getstar = Tant que isflow(f)
 f = get(f)
end

on ne change pas la façon dont les futurs sont traités en mémoire, juste la façon avec laquelle on intéragit avec. 

---

### Dataflow explicit futures --- exemple

```ruby [|2,4,6,13,14]
active class CachedBroker
    def compute(arg: Arg): Flow[Result]
        if cache.has(arg) then
            cache.get(arg)
        else
            get_worker()!compute(arg)
        end
    end
end

def main(): unit
    val broker = new Broker
    val f: Flow[Result]] = broker!!compute(new Arg)
    val result = get*(f)
end
```

Notes:
permet aussi de typer l'exemple du cache de précédement, même en version compliquée

---

### 2 solutions

- **délégation**: réduit les chaines de futurs
- **dataflow explicit futures**: change l'interface dans le language

Notes:
`forward` en DeF => `forward*`

---

# Mes contributions

---

## Mes contributions

**partie pratique, hors implémentation directe**

- Rétro-compatibilité : Fut-on-Flow
- Performance: benchmarks

---

## Fut-on-Flow

Implémenter Control-flow futures en Encore

Motivation:
- Avoir les 2 solutions en même temps
- Implémenter Flow sur Fut difficile

Notes:
Les flows ça a l'air cool
On veut avoir les deux en même temps
Il faut choisir lequel implémenter en premier
L'autre sens a déjà été essayé, sans succès
Aujourd'hui pas de preuve que l'autre sens marche

---

## Fut-on-Flow --- comment ?

**Difficulté:** arrêter l'opérateur `get*`
<!-- .element: class="fragment" -->

![fut-on-flow schema](./schemas/fut-on-flow.svg)
<!-- .element: class="fragment" -->

`Box`, `box(t)`, `unbox(b)`
`\[ \texttt{Fut}[T] := \texttt{Box}[\Flow[T]] \]`
<!-- .element: class="fragment" -->

---

## Fut-on-Flow --- implémentation

Les opérations suivent:
- `get(f)` := `get*(unbox(f))`
- `a!m()` := `box(a!!m())`

- `forward(f)` := `forward*(unbox(f))`

---

## Fut-on-Flow --- limites

Librairie en Encore 

`$\implies$` on ne peut pas surcharger `a!m()`

```ruby
def main():
    let f = call_((new Broker)!!compute(arg))
    let x = get_(get_(f))
end
```

Mais on est à un sucre syntaxique près
<!-- .element: class="fragment"-->

---

## Fut-on-Flow --- conclusion

- On a réussi!
- On pense que c'est plus simple dans ce sens

Notes:
Pour implémenter des futurs, d'abord implémenter des flows, puis implémenter les fut à l'aide d'une librairie plus du sucre syntaxique

---

## Benchmarks

**Goals:** 
- Are we fast yet ?
- Écrire des exemples qui tournent avec DeF

Notes:
- En plus, ça a permis de vérifier que tout marchait bien, de débogguer / d'optimiser

---

## Benchmarks --- liste

1. LinkedList
2. WordCounter
3. Ackermann
4. Factorielle auto-récursive
5. Factorielle avec création d'acteur

Notes:
1. La linkedlist évalue le temps pris par la synchronisation d'une longue chaine
2. Le wordcounter évalue le temps pris par de nombreuses synchronisation parallèle sur des courtes chaines
3. Ackermann évalue la fonction d'ackermann avec création d'acteur sur des chaines de taille variable
4. La factorielle évalue la synchronisation d'une longue chaine sur un seul acteur
5. La factorielle avec création d'acteur évalue la synchronisation d'une longue chaine avec création d'acteurs

Je présente les 2 premiers, les 3 derniers sont dans le rapport, et leurs résultats confirment ceux de la LinkedList.

---

## LinkedList

Exemple:
```ruby
active class Node:
    state: int
    next: Maybe[Node]

    def sum(acc: int): int
        match this.next with 
            case Nothing    => acc + this.state
            case Just(node) => get(node!sum(acc + this.state))
        end
    end
end
```

---

## LinkedList --- results

| Future used | Average running time |
|---|--:|
| `Fut` with `forward` | 16 ms |
| `Fut on Flow` + `forward` | 16 ms |
| `Flow` with `forward*` | 16 ms |
| `Flow` | 26 ms |
| `Fut` | 47 ms |
| `Fut on Flow` | 48 ms |


---

## WordCounter

- Compte le nombre de mots dans un texte
- Un broker répartit des insertions dans une HashTable parallélisée

---

## WordCounter --- résultats

| Future used | Average running time |
|---|--:|
| `OneWay` | 26 ms |
| `Fut` | 35 ms |
| `Flow` | 35 ms |
| `Fut on Flow` | 35 ms |
| synchrone | 46 ms |
<!-- .element: class="fragment"-->

Notes:
- un des premiers benchmarks stables
- le code compte le nombre de mots dans un texte de façon distribuée

---

## Benchmarks --- conclusion

Notre meilleur choix:

Dataflow Explicit Futures
+
Delegation

---

## Conclusion --- contributions

Il est possible d'implémenter les control-flow futures sur les dataflow explicit futures.

![benchmarks results](./schemas/Benchresults.svg)

Notes:
Insister sur les contribs
Et les nouvellement implémentés sont aussi performants que les premiers.
Autres résultats:
 - Fut-on-Flow ~= Fut
 - One-way messages plus rapide que tout

---

## Conclusion

`forward*` `$\iff$`  `return`

`\(\rightarrow\)` `forward*` devient une optimisation automatique
Notes:
- on peut remplacer `return` par `forward*`
- le compilateur peut faire le remplacement
- le programmeur n'a pas besoin de connaître forward

---

# Merci

# Questions ?

---

## Références

- Kiko Fernandez-Reyes, Dave Clarke, Ludovic Henrio, Einar Broch Johnsen, and Tobias Wrigstad. “Godot: All the Benefits of Implicit and Explicit Futures”. July 2019. [source](https://hal.archives-ouvertes.fr/hal-02302214)

- Nicolas Chappe, Ludovic Henrio, Amaury Maillé, Matthieu Moy, and Hadrien Renaud. “An Optimised Flow for Futures: from Theory to Practice”. Article being finalized. Oct. 2020.

_(en autres)_

